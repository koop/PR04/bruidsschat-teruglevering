## **INFORMATIE OVER DE TER BESCHIKKING GESTELDE CONTENT VAN DE BRUIDSSCHAT**
### **Introductie**
- De zip-bestanden in deze GIT-repository bevatten de STOP/TPOD en STTR bruidsschat content van de gemeenten en waterschappen.
- De STOP/TPOD zips worden ook wel &quot;terugleverpakket&quot; genoemd. De naam van de zip begint met &#39;Terugleverpakket&#39; gevolgd door de BG-code van het betreffende bevoegd gezag waarvan de content is.
- Een &quot;terugleverpakket&quot; is bedoeld om de STOP/TPOD content in de eigen systemen te laden en/of te bekijken.
- Inmiddels zijn ook de definitieve &quot;terugleverpakketten&quot; beschikbaar op deze GIT (zie Update 24-05-2023).
- _N.B.: Het is nadrukkelijk **niet** de bedoeling om deze content te gebruiken om het in LVBB/DSO te laden: het DSO-programma verzorgt het laden t.b.v. de initiële vulling op productie. (Uitzondering zijn de waterschappen die zelf zorgen voor de publicatie van hun initiële waterschapsverordening.)_


### **UPDATE 24-05-2023: de definitieve STOP/TPOD terugleverpakketten zijn beschikbaar.
De definitieve bruidsschat voor de gemeenten is geladen op productie en daarmee zijn ook alle definitieve terugleverpakketen beschikbaar.
- Zie de map &quot;Gemeenten-STOPTPOD-mei2023versie-prod&quot; voor de terugleverpakketten voor de gemeenten
- Zie de map &quot;Waterschappen-STOPTPOD-mei2023versie&quot; voor de terugleverpakketten voor de waterschappen

_**Wijzigingen ten opzichte van de juni 2022 versie**_

Zie ook de release notes bij de bruidsschat van de gemeenten die op productie geladen zijn: 230523 Releasenote Bruidsschat op productie.pdf (ook in de root van dezet GIT).

Met het Koninklijk Besluit van 5 april 2023 tot vaststelling van het tijdstip van inwerkingtreding van o.a. het Invoeringsbesluit Omgevingswet [(Staatsblad 2023, 113)](https://zoek.officielebekendmakingen.nl/stb-2023-113.html), heeft de bruidsschat een datum inwerkingtreding gekregen (01-01-2024).

Hierdoor was het noodzakelijk om een aantal gegevens aan te passen t.o.v. de bruidsschat die afgelopen juni 2022 beschikbaar was gesteld.

In de STOP consolidaties.xml:
- De datum &#39;bekendOp&#39; is aangepast in 2023-04-07: dit is de datum publicatie van Staatsblad 2023, 113.
- Het Work-id van de consolidatie is aangepast in /akn/nl/act/gemeente/2024/CVDRXXXXXX.
- Het Expression-id van de consolidatie is aangepast in /akn/nl/act/gemeente/2024/CVDRXXXXXX/nld@2024-01-01.
- De &#39;vanaf&#39;-datum van &#39;juridischWerkendOp&#39; is aangepast in 2024-01-01
- De &#39;vanaf&#39;-datum van &#39;geldigOp&#39; is aangepast in 2024-01-01
- Voor de waterschappen gelden dezelfde aanpassingen, alleen daar waar &#39;gemeente&#39;, moet dan &#39;waterschap&#39; gelezen worden.

In de WTI (wettechnische informatie):
- Staatsblad 2023, 113 is toegevoegd.
- Verder zijn er dezelfde aanpassingen als voor bekendOp, juridischWerkendOp, geldigOp en het Work-id van de consolidatie (deze WTI wordt alleen geladen voor de setjes die door KOOP geladen worden).

In de diverse ow-xml bestanden:
- In de locaties.xml is de geldigOp datum van het ambtsgebied aangepast in 2024-01-01
- In de ow-bestanden van de 7 Zinkassen gemeenten (Boxtel, Horst aan de Maas, Maasgouw, Oisterwijk, Roermond, Tilburg, Vught) zijn, op verzoek van de plansoftwareleveranciers\*, de volgende additionele wijzigingen gedaan:
	- In de locaties.xml is de gebiedengroep en het gebied verwijderd zodat alleen het ambtsgebied overblijft.
	- In de regeltekst.xml is op 12 plekken de verwijzing naar nl.imow-gmXXXX.gebiedengroep.ZinkassenGebiedKempen vervangen door een verwijzing naar het ambtsgebied.
	- Het gml-bestand is verwijderd uit de set.
	- De diverse manifesten zijn op deze wijzigingen aangepast.

\*) juridisch zullen de betreffende artikelen nog steeds het in bijlage III aangewezen werkingsgebied "Zinkassengebied De Kempen" hebben, zoals bekendgemaakt.


_**Wat is er niet gewijzigd**_

- De tekst van de initiële regelingversie is niet gewijzigd.
- Alle overige id’s en metagegevens zijn niet gewijzigd, zoals het Work-id van de regeling (/akn/nl/act/gmXXXX/2020/omgevingsplan), de wIds en de id’s van de ow-objecten.
- Buiten bovenstaande aanpassingen, zijn de ow-xml bestanden verder niet gewijzigd.
- Voor de waterschappen moet bij &#39;gm&#39; of &#39;omgevingsplan&#39; resp. &#39;ws&#39; of &#39;waterschapsverordening&#39; gelezen worden.
- Het CVDR-nummer (in het AKN-id van de consolidatie) in de consolidaties.xml in de terugleverpakketten is nog steeds een dummy-nummer. Het echte CVDR-nummer is bij het laden toegekend. De initiële regeling is op lokaleregelgeving.overheid.nl te zien vanaf 01-01-2024.
- De paar fouten die in de tekst van het omgevingsplan staan (ook al in de juni 2022 versie, maar dus ook nog steeds in de mei 2023/PROD versie), komen nog steeds voor en hiervoor is géén opmerking (met STOP-element &#39;Redactioneel&#39;) toegevoegd omdat dit niet is toegestaan in de TPOD:
	- De verwijzing in artikel 22.120, lid 1, aanhef,  &quot;..., bedoeld in artikel 22.113, ...&quot; had moeten zijn &quot;..., bedoeld in artikel 22.115, ...&quot;
	- De verwijzing in de toelichting bij artikel 22.35, onderdeel j &quot;(artikel 22.29, derde lid, en 22.30).&quot; had moeten zijn &quot;(artikel 22.29, eerste lid, onder c, en 22.30).&quot;
	- De toelichting op het nieuwe artikel 22.62a is met een foute kop toegevoegd (nml als &#39;derde lid&#39; van artikel 22.62).



### **UPDATE 09-08-2022: een aantal STTR bestanden in de gecomprimeerde Bruidsschat set (vereenvoudigde-sttr-gemeentes.zip) waren niet valide en dit is nu hersteld**

### **UPDATE 03-08-2022: Uitleverschema is toegevoegd (lvbb-stop-uitlevering.xsd)**

### **UPDATE 01-07-2022: STTR-bestanden voor teruglevering zijn toegevoegd**

### **UPDATE 28-06-2022: Nieuwe sets Bruidsschat zoals deze zijn geladen op de PRE-omgeving per 28 juni voor teruglevering zijn toegevoegd**


### **UPDATE 05-06-2022: Nieuwe sets Bruidsschat inclusief interne verwijzingen (voor laden op PRE eind juni) voor teruglevering zijn toegevoegd**
_**Wijzigingen ten opzichte van de vorige versie**_

N.B.: de interne verwijzingen zijn alleen opgenomen in de omgevingsplannen en niet in de waterschapsverordeningen in tegenstelling tot wat de commit-message suggereert.

De wijzigingen op het omgevingsplan en de waterschapsverordening zijn op hoofdlijnen hieronder weergegeven:

- Omgevingsplan
  - Wijzingen n.a.v. het verbeterblad voor Aanvullingsbesluit bodem Omgevingswet (stb-2021-98-n2), Verzamelbesluit (stb-2022-172) en Besluit tijdelijke overbruggingsregeling windturbineparken (stb-2022-181) zijn geconsolideerd:
    - naast wijzigingen in bestaande tekst (artikelen, leden, artikelgewijze toelichting e.d.), zijn ook artikelen en leden toegevoegd, zijn onderdelen vernummerd en zijn onderdelen vervallen.
    - oa hierdoor zijn er ook wijzigingen in de wIds: zie voor een overzicht van de verschillen in de wIds "Verschillen wIds.xlsx", blad "wIds-OMGPLAN"
    - ook is hierdoor e.e.a. veranderd in de ow-activiteiten: zie voor een overzicht van de verschillen voor de ow-activiteiten "Verschillen ow-act.xlsx", blad "ow-activiteiten-OMGPLAN"
    - verder is het id van het ow-ambtsgebied gewijzigd en veel van de id's in de regeltekst.xml zijn veranderd (Regeltekst, RegelVoorIedereen, ActiviteitLocatieaanduiding) doordat een activiteit verwijderd is en leden/artikelen zijn toegevoegd: ga ervan uit dat deze dus gewijzigd zijn.
  - Het dubbele begrip in de begrippenlijst (bijlage I), NEN-EN-ISO 15680, is verwijderd
  - De fouten in de volgorde van de begrippen in de begrippenlijst (bijlage I) zijn hersteld
  - De lettering (LiNummer) in de begrippenlijst van bijlage II is verwijderd
  - Gradentekens in de lijstnummering op het tweede niveau zijn verwijderd.
  - Interne referenties naar regelingonderdelen zijn toegevoegd t/m niveau lid (er zijn geen interne referenties toegevoegd voor begrippen)
  - Paar kleine tekstuele fouten mochten we eruit halen:
    - op twee plekken is ontbrekende punt achter lijstnummering toegevoegd: bij artikel 22.241, lid 1, item b, item 4 en bij artikel 22.247, lid 1, item b, item 4
    - artikelsgewijze toelichting van artikel 22.41, tweede lid: "»een omvang alsof zij bedrijfsmatig is»" is aangepast in "«een omvang alsof zij bedrijfsmatig is»"
    - artikel 22.70, lid 2, aanhef, "bedoeld in de artikelen 22.63 tot en met, 22.67 en 22.69," komma achter tot en met verwijderd
    - toelichting bij artikel 22.2, "...de onderdelen van de artikelen 22.28 22.38, 22.276, 22.277, 22.279 tot en met 22.282 en 22.284 die....": ontbrekende komma tussen 22.28 22.38 toegevoegd.
    - toelichting bij artikel 22.98, eerste lid, "...of de eerbiedigende werking in artikel 22.99": afsluitende punt toegevoegd.
  - Verder zijn er nog enkele andere fouten ontdekt in de tekst, waarvan nog uitgezocht wordt of hier een redactionele opmerking in de tekst voor toegevoegd mag worden:
    - De verwijzing in artikel 22.120, lid 1, aanhef,  "..., bedoeld in artikel 22.113, ..." had moeten zijn "..., bedoeld in artikel 22.115, ..."
    - De verwijzing in de toelichting bij artikel 22.35, onderdeel j "(artikel 22.29, derde lid, en 22.30)." had moeten zijn "(artikel 22.29, eerste lid, onder c, en 22.30)."
    - De toelichting op het nieuwe artikel 22.62a lijkt met een foute kop toegevoegd te zijn ('derde lid' van artikel 22.62): wordt nog nagevraagd bij BZK.

- Waterschapsverordening
  - Wijzingen n.a.v. het Verzamelbesluit (stb-2022-172) zijn geconsolideerd:
    - naast wijzigingen in bestaande tekst (artikelen, leden, artikelgewijze toelichting e.d.), is ook een lid toegevoegd.
    - oa hierdoor zijn er ook wijzigingen in de wIds: zie voor een overzicht van de verschillen in de wIds "Verschillen wIds.xlsx", blad "wIds-WSV"
    - er zijn geen veranderingen in de namen en id's van de ow-activiteiten: zie overzicht "Verschillen ow-act.xlsx", blad "ow-activiteiten-WSV"
    - verder is het id van het ow-ambtsgebied gewijzigd en veel van de id's in de regeltekst.xml zijn veranderd (Regeltekst, RegelVoorIedereen, ActiviteitLocatieaanduiding) doordat een lid is toegevoegd: ga ervan uit dat deze dus gewijzigd zijn.
  - De fouten in de volgorde van de begrippen in de begrippenlijst (bijlage I) zijn hersteld
  - Gradentekens in de lijstnummering op het tweede niveau zijn verwijderd.


### **UPDATE 06-10-2021: Nieuwe sets Bruidsschat (oktober-versie) voor teruglevering zijn toegevoegd**
_**Wijzigingen ten opzichte van de vorige versie zijn terug te vinden in de releasenotes van de Bruidsschat.**_

### **UPDATE 21-04-2021: Nieuwe sets Bruidsschat voor teruglevering en PDF voorbeelden zijn toegevoegd**
_**Wijzigingen ten opzichte van de vorige versie**_

De terugleverpakketten (downloadbare versies) van de juridische regels bruidsschat zijn geupdate, zodat deze corresponderen met de versie die nu op de oefenomgeving van het DSO is geladen. De belangrijkste wijzigingen ten opzichte van de vorige versie zijn: 
-	Voor de waterschapsverordening zijn de "aangewezen oppervlaktewaterlichamen" en "niet-aangewezen oppervlaktewaterlichamen" als specifieke werkingsgebieden toegevoegd.
-	Voor een 7-tal gemeenten is het specifieke werkingsgebied "De Kempen" toegevoegd.
-	De tekst, structuur en nummering volgt het vastgestelde Invoeringsbesluit, het Aanvullingsbesluit Bodem en het Aanvullingsbesluit Geluid.
-	De nummering van sommige artikelen en leden is aangepast omdat enkele artikelen zijn verwijderd of toegevoegd. 

_**Waar moet u rekening mee houden als u oefent**_

-	De content heeft een CONCEPT status en is dus nog niet definitief.
-	Sommige bestanden bevatten nog notities. Dit herkent u als tekst tussen <!-- -->. U kunt deze commentaren negeren, dit wordt gebruikt ter uitleg en afstemming in het maakproces.
-	Het ambtsgebied maakt geen onderdeel uit van de terugleverpakketten. Ambtsgebieden worden niet aangeleverd door een bevoegd gezag, maar zullen aanwezig zijn in DSO-LV. Het DSO-programma zorgt ervoor dat het ambtsgebied beschikbaar is in DSO-LV.
-	Om van het ambtsgebied in DSO-LV gebruik te kunnen maken moet dit in het IMOW-locatiebestand worden aangegeven. Dat is gedaan voor de bruidsschat (zie hiervoor het document [bgcode]Locaties.xml). Hierbij dient op de plek van [bgcode] de code van het betreffende bevoegde gezag te worden ingevuld. Bijvoorbeeld gm0175 of ws0636.
-	Houd er rekening mee dat enkele metagegevens nog kunnen wijzigen. Enkele metagegevens bevatten nu nog dummy waarden.

_**PDF voorbeeldbestanden**_

We hebben PDF's gemaakt van de verschillende varianten van bruidsschat:
- voorbeeld-pdf-omgevingsplan_variantKEMPEN28.pdf: de variant voor de gemeenten waarvan het ambtsgebied geheel in gebied De Kempen ligt (28).
- voorbeeld-pdf-omgevingsplan_variantZINKASSEN7.pdf: de variant voor de gemeenten die voor een gedeelte in gebied De Kempen liggen (7).
- voorbeeld-pdf-omgevingsplan_variantREST.pdf: de variant voor alle overige gemeenten (317).
- voorbeeld-pdf-waterschapsverordening.pdf: de waterschapsverordening. Zit nauwelijks verschil in, alleen in Bijlage II > is beschreven in Bijlage II.

### **UPDATE 15-04-2021: STTR-bestanden voor teruglevering zijn toegevoegd**
Voor deze bestanden geldt:
In elk bestand staan de placeholders ${oin} en ${code}. Om de bestanden te gebruiken moet de oin placeholder vervangen worden door het oin van het bevoegd gezag waarvoor je het bestand wil uploaden. Dit zal normaal gesproken op 3 plekken zijn:
- Het xmlns element
- Het namespace element
- In de href van het bedr:functioneleStructuurRef element

De code placeholder moet vervangen worden door:  “nl.imow-<gemeente-/waterschapscode>.activiteit.”. Deze staat ook in de href van het bedr:functioneleStructuurRef element.


### **UPDATE 14-01-2021: FIX op dubbele ID's juridische regels**
_**Wat is er gevonden?**_

In het OW-regeltekst bestand in de bruidsschat-sets, komen meerdere exemplaren van dezelfde RegelVoorIedereen (JuridischeRegel) voor, m.a.w. RegelVoorIedereen ID’s (JuridischeRegel-ID’s) komen meerdere keren voor.

Welke gevolgen heeft dit voor opslag van de content in de verschillende systemen
- Voor LVBB: geen.
  - LVBB slaat geen OW-bestanden op en de STOP-regeling(versie) is goed opgeslagen.
- Voor RTR (toepasbare regels): geen.
  - Alle activiteiten zijn goed opgeslagen in RTR, incl. de juridische grondslag, locatie, begin- en einddatum.
- OZON/Viewer: voor twee juridische regels in het Omgevingsplan en voor één juridische regel in de waterschapsverordening ontbreken 1 of meer activiteiten.
Elk exemplaar van een juridische regel is als versie van die juridische regel opgeslagen. Het laatste exemplaar is opgeslagen als de huidige versie van de juridische regel. Hierdoor zie je één geannoteerde activiteit in de viewer, terwijl dit er meer hadden moeten zijn voor de betreffende juridische regels:
  - Omgevingsplan, artikel 22.28: je ziet hier één activiteit geannoteerd in de viewer, terwijl dit er 10 hadden moeten zijn;
  - Omgevingsplan, artikel 22.35: je ziet hier één activiteit geannoteerd in de viewer, terwijl dit er 3 hadden moeten zijn.
  - Waterschapsverordening, artikel 3.1 lid 1: je ziet hier één activiteit geannoteerd in de viewer, terwijl dit er 2 hadden moeten zijn.
  - Alle overige juridische regels zijn correct opgeslagen, omdat het laatste exemplaar alle benodigde informatie (annotaties) bevat.

_**Wat is er gewijzigd?**_

Er is besloten de bruidsschat-setjes in OZON te herstellen. De “terugleversets” voor de Bevoegd Gezagen zijn hersteld: de terugleversets op de “Bruidsschat terugleveren Gitlab” zijn vervangen door de herstelde sets.

**Uitdrukkelijk verzoek om deze laatste versie van de terugleversets te gebruiken bij het oefenen!**


### **Status en bron van de content**
- De content heeft een CONCEPT status en is dus nog niet definitief.
 - Na het bekendmaken van het Invoeringsbesluit, Aanvullingsbesluit Bodem en Aanvullingsbesluit Geluid, is de definitieve content voor de bruidsschat bekend.
 - Daarna zullen de definitieve bruidsschat sets worden gemaakt en deze zullen weer via deze weg beschikbaar worden gesteld.
- Sommige bestanden bevatten nog notities (commentaren): de tekst tussen \<!-- --\>
 - Dit kan genegeerd worden: het is/wordt gebruikt t.b.v. uitleg/afstemming in het maakproces.
 - De definitieve content zal deze commentaren niet meer bevatten.
- De bron die is gebruikt voor de content in deze repository is een tussenversie, en dus een niet-definitieve versie, van het Invoeringsbesluit.
- Voor nu zijn alle regels gekoppeld aan het ambtsgebied.
 - Het ambtsgebied maakt geen onderdeel uit van de terugleverpakketjes: ambtsgebieden worden niet aangeleverd door een BG, maar zullen aanwezig zijn in DSO-LV. Het DSO-programma zorgt ervoor dat het ambtsgebied beschikbaar is in DSO-LV.
- Welke wijzigingen worden verwacht in de definitieve content?
 - Specifieke werkingsgebieden worden nog toegevoegd.
 - Voor de waterschapsverordening zijn dat de &quot;aangewezen oppervlaktewaterlichamen&quot; en &quot;niet-aangewezen oppervlaktewaterlichamen&quot;.
 - Voor enkele gemeenten betreft dat &quot;Gebied De Kempen&quot;.
 - Tekst, structuur, nummering naar aanleiding van vastgestelde IB, AB Bodem en AB Geluid.
 - Met b.v. AB Bodem en AB Geluid worden er nog bodemregels en geluidsregels toegevoegd.
 - Omgevingsplan: &quot;HOOFDSTUK 2 tot en met HOOFDSTUK 21&quot; wordt opgesplitst in de verschillende hoofdstukken. (Wel blijven ze allen &#39;gereserveerd&#39;.)
 - Omdat enkele artikelen worden verwijderd/toegevoegd zal de nummering aangepast worden. Voor de waterschapsverordening lijkt het te blijven bij enkele leden die toegevoegd worden en tabellen die opgesplitst/toegevoegd worden.
 - Enkele metagegevens kunnen nog wijzigen:
 - b.v. definitieve afstemming met de koepels (VNG &amp; UvW) en PR34.
 - Enkele metagegevens bevatten nu nog &quot;dummy waarden&quot;, zie hiervoor &quot;extra informatie over de STOP/TPOD content&quot;.
 - Verder wordt in de ketensessie over gemeentelijke herindeling ook de impact op de bruidsschat meegenomen.
### **Extra informatie over de STOP/TPOD content**
- Een zip bevat de volgende bestanden:
 - Consolidaties.xml: STOP consolidatie met de initiële regelingversie van het omgevingsplan/de waterschapsverordening
 - Activiteiten.xml: bestand met de OW-Activiteit objecten behorend bij het initiële omgevingsplan/de initiële waterschapsverordening 
 - Regeltekst.xml: bestand met de OW-Regeltekst en – RegelVoorIedereen objecten behorend bij het initiële omgevingsplan/de initiële waterschapsverordening
 - Regelingsgebied.xml: het OW-Regelingsgebied object behorend bij het initiële omgevingsplan/de initiële waterschapsverordening
 - Manifest.xml: opsomming van alle bestanden in de zip.
 - Manifest-ow.xml: opsomming van alle OW-bestanden in de zip.
  
  
- Consolidaties.xml:
 - ConsolidatieIdentificatie: het AKN-FRBRWork-id van de consolidatie is een dummy-id. Deze wordt tegenwoordig door de LVBB gemaakt. De ConsolidatieIdentificatie hoeft niet door de BG/leverancier opgeslagen te worden omdat deze pas ontstaat bij de LVBB en geen onderdeel uitmaakt van een Besluit of een Regeling(versie).
 - Onder Toestanden zijn allerlei tijdstempels opgenomen: ook dit zijn allen &quot;dummy-data&quot;. Dit is natuurlijk omdat de exacte data nog niet bekend zijn en omdat, voor een goede werking van de keten, op dit moment nog nodig is dat de datum &quot;juridischWerkend vanaf&quot; in het verleden moet liggen.
 - Onder RegelingVersie bevindt zich de informatie over de initiële regelingversie van het omgevingsplan/de waterschapsverordening die de BG/leverancier in het eigen systeem kan laden:
 - ExpressionIdentificatie bevat de BG-AKN-identificatie van het FRBRWork (&#39;de regeling&#39;) en van de FRBRExpression (&#39;de regelingversie&#39;). Deze zullen dus hetzelfde zijn in de definitieve initiële versie van het omgevingsplan/de waterschapsverordening.
 - RegelingCompact: bevat de bruidsschatregels (de inhoud van de regelingversie), in STOP tekstmodel &#39;RegelingCompact&#39; opzet.
 - Onder AnnotatieBijToestand bevindt zich de RegelingMetadata die hoort bij het Work van het bevoegd gezag. Daarmee hoort het ook bij de Toestand, maar in eerste instantie is het onderdeel van het BG-Work en dus is dit ook iets dat je als BG/leverancier zou laden in het eigen systeem.
 - De grondslagen zijn hierin nog &quot;uit gecommentarieerd&quot;, omdat ze nog wat problemen geven in de keten, en hierin zijn ook nog &quot;dummy-Uri&#39;s&quot; gebruikt.
