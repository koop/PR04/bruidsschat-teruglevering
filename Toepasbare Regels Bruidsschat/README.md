Uitleg mappenstructuur �bruidsschat-teruglevering / Toepasbare Regels Bruidsschat�

In de map �Toepasbare Regels Bruidsschat� vindt u de volgende submappen:
-	01 STTR-bestanden � juli 2022: deze map bevat de STTR-bestanden Bruidsschat die is opgeleverd in juli 2022.
-	02 STTR-bestanden � januari 2023 (inclusief MoM update): deze map bevat de STTR-bestanden van de Bruidsschat die is opgeleverd in januari 2023. Hierin is voor een aantal activiteiten het toepassingsbereik toegevoegd aan de toepasbare regels voor Maatregelen op Maat. Tevens is een extra check toegevoegd voor de activiteit 'Kleinschalig graven (25 m3 of minder) in bodem met een kwaliteit boven de interventiewaarde bodemkwaliteit'. Deze map vervangt volledig de STTR-bestanden van juli 2022.
-	03 STTR-bestanden � juli 2023 (finale oplevering): deze map bevat de finale oplevering van de STTR-bestanden van de Bruidsschat. Deze versie is tevens op de productie omgeving van het DSO gezet. Deze map vervangt volledig de STTR-bestanden van januari 2023.
-	04 Ontwerpdocumentatie Bruidsschat 01-07-2022: deze map bevat de ontwerpdocumentatie van de checks uit de Bruidsschat en van het aanvraagformulier voor de binnenplanse omgevingsvergunning omgevingsplanactiviteit bouwwerken, ook wel de �Bouwactiviteit (omgevingsplan)� genoemd.
-	05 Aanpassingen na juli 2023 (finale oplevering): deze map bevat aanpassingen van de STTR-bestanden van de Bruidsschat die n� de finale oplevering zijn opgeleverd. Dit kunnen twee soorten aanpassingen zijn:
-	Een fout die is verbeterd.
-	Een aanpassing in een Bbl-check naar aanleiding van een wijziging in de juridische regels in het Bbl (is alleen van toepassing op de Bruidsschat gemeenten).
Per verbetering wordt een aparte submap gemaakt met daarin uitleg over de aanpassing (vaak in de vorm van een gewijzigd ontwerpdocument) en zo nodig een aangepast STTR-bestand. Gemeenten en waterschappen dienen zelf te zorgen voor het doorvoeren van een aanpassing in de productie-omgeving van het DSO.
-	06 Database toepasbare regels Bruidsschat: deze map bevat de database inhoud in twee formaten:
-	Microsoft SQL Server database dump format (.bak file)
-	SQL text file
In de map staat ook de disclaimer die hierbij hoort.
-	07 Maatregelen op Maat periodiek Excel: deze map bevat het Excel-bestand met alle bruidsschat maatregelen waarin alle verbeterpunten zijn verwerkt. Deze Excel wordt begin van de maand bijgewerkt, indien er de voorafgaande maand wijzigingen zijn geweest.  
