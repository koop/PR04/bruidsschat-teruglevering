﻿2023-10 versie van de bruidsschat-bestanden

Deze set is gemaakt in het kader van de definitieve oplevering van de bruidsschat-bestanden op PROD. 
T.o.v. de vorige set bestanden is in deze set aangepast:

Een viertal STTR-bestanden bleek toch nog een imgur-link te bevatten. Die links zijn nu gewijzigd in een iplo-link (zowel in de volledige als in de gecomprimeerde versies van de STTR-bestanden). Ook is de zogeheten ALT-tekst (beschrijving bij de afbeelding) toegevoegd.
	- Gemeentes:    
		- Maatregelen - PolyesterharsVerw
	- Waterschappen:
		- IV Vergunning - OnttrekkenGrondwater
		- IV Vergunning - OnttrekkenOppervlaktewater
		- IV Vergunning - OverigeActBepWaterstWWs
	
Één STTR bestand in de vereenvoudigde set van de gemeentes bevatte nog een oin van de gemeente Rotterdam in plaats van de ${oin} placeholder
    	- Gemeentes:
		- Maatregelen - GeurProdAndere Act gecomprimeerd

Verder is de set inhoudelijk gelijk aan die van 2023-07

De excel met het detail overzicht van de aanpassingen is bijgewerkt. Het viertal STTR-bestanden bleek daar niet in te zitten. Deze zijn toegevoegd in het eerste tabblad van de excel.

Tot slot: op verzoek is ook een document toegevoegd met uitleg over maatregelen op maat.